package com.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="user")
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int user_id;
	private String user_name;
	private String user_passwd;
	@Transient
	private String conf_passwd;
	
	@OneToMany(mappedBy="user", cascade=CascadeType.ALL)
	private List<Pets> pets;
	

	public User() {
		super();
	
	}

	public User(String user_name, String user_passwd,String conf_passwd) {
		super();
	
		this.user_name = user_name;
		this.user_passwd = user_passwd;
		this.conf_passwd=conf_passwd;
	
	}

	

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getUser_passwd() {
		return user_passwd;
	}

	public void setUser_passwd(String user_passwd) {
		this.user_passwd = user_passwd;
	}

	public List<Pets> getPets() {
		return pets;
	}

	public void setPets(List<Pets> pets) {
		this.pets = pets;
	}

	public String getConf_passwd() {
		return conf_passwd;
	}

	public void setConf_passwd(String conf_passwd) {
		this.conf_passwd = conf_passwd;
	}

	
	

}
