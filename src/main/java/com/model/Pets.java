package com.model;

import javax.persistence.*;

@Entity
@Table(name="pets")
public class Pets {
	@Id
	//@GeneratedValue(strategy=GenerationType.AUTO)
	private int pet_id;
	public int getPet_id() {
		return pet_id;
	}

	public void setPet_id(int pet_id) {
		this.pet_id = pet_id;
	}

	private String pet_name;
	@Column(name="pet_age",nullable=true)
	private int pet_age;
	private String pet_place;
	 
	@ManyToOne
	@JoinColumn(name="PET_OWNERID")
	private User user;

	

	public String getPet_name() {
		return pet_name;
	}

	public void setPet_name(String pet_name) {
		this.pet_name = pet_name;
	}

	public int getPet_age() {
		return pet_age;
	}

	public void setPet_age(int pet_age) {
		this.pet_age = pet_age;
	}

	public String getPet_place() {
		return pet_place;
	}

	public void setPet_place(String pet_place) {
		this.pet_place = pet_place;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Pets( String pet_name, int pet_age, String pet_place) {
		super();
	
		this.pet_name = pet_name;
		this.pet_age = pet_age;
		this.pet_place = pet_place;

	}

	public Pets() {
		super();
	
	}
	
	
	
	

}
