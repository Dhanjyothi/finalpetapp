package com.main;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.model.Pets;
import com.model.User;

public class MainApp {
	public static void main(String[] args) {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml"); 
		
		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();		
		
		Transaction t=session.beginTransaction();
		
		Pets pet1=new Pets("Jack",2,"CH");
		Pets pet2=new Pets("Jim",4,"KR");
		
		User user=new User("Dhivya","1234","1234");
		List<Pets> list=new ArrayList<Pets>();
		
		pet1.setUser(user);
		pet2.setUser(user);
		
		list.add(pet1);
		list.add(pet2);
		
		user.setPets(list);
		
		session.save(user);
		t.commit();
		
	}

}
