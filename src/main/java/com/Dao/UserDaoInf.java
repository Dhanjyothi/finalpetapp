package com.Dao;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.model.Pets;
import com.model.User;

public interface UserDaoInf {

	void saveUser(User user);

	List<Pets> home();

	List<Pets> petList(int id);

	void savepet(int id,Pets pets);

	int checkUser(String name, String passwd);

}
