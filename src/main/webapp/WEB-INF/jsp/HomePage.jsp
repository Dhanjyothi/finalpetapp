<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<title>Home Page</title>
</head>
<body>
	<!-- <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">WebSiteName</a>
    </div>
    
    <ul class="nav navbar-nav navbar-right">
      <li><a href="addPet"><span class="glyphicon glyphicon-user"></span> Add pet</a></li>
      <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
    </ul>
  </div>
</nav> -->

	<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">PetShop</a>
		</div>
		<ul class="nav navbar-nav">
			<li class="active"><a href="home">Home</a></li>
			<li><a href="mypet">My Pet</a></li>
			<li><a href="addPet">Add Pet</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="logout"><span class="glyphicon glyphicon-log-in"></span>
					Logout</a></li>
		</ul>
	</div>
	</nav>

	<div align="center" class="table-responsive">

		<form:form action="buyPet" modelAttribute="pets">
			<table class="table table-bordered table-sm" style="width: 50%; margin-left: 200px;">


				<tr>
					<th>Pet ID</th>
					<th>Pet Name</th>
					<th>Pet Age</th>
					<th>Pet Place</th>
					<th>Buy</th>

				</tr>
				<c:forEach var="ls" items="${list}">
					<tr>

						<td>${ls.pet_id}</td>

						<td>${ls.pet_name}</td>

						<td>${ls.pet_age}</td>


						<td>${ls.pet_place}</td>


						<td><a href="addRoom">Buy</a></td>
					</tr>
				</c:forEach>
			</table>
		</form:form>

	</div>
</body>
</html>