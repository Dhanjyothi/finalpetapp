package com.pettest;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages="com.pettest")
public class TestBeanConfig {

}
